<?php

namespace cnn;

class ConnectionMssql{

	private $dbUrl 	= DB_URL; 
	private $dbName = DB_NAME;
	private $dbUser = DB_USER;
	private $dbPwd 	= DB_PWD;

	private $query;
	private $id;

	function connection() {
		$this->id = odbc_connect("DRIVER={SQL Server}; SERVER=$this->dbUrl;  DATABASE=$this->dbName; Client_CSet=UTF-8; Server_CSet=ISO-8859-1", $this->dbUser,$this->dbPwd);
		if (! $this->id) {
			die ( "Erro na conexão com a base de dados!" );
		}
	}

	private function closeConnection() {
		odbc_close ( $this->id );
		return;
	}

	public function getIdConection() {
		$this->connection ();
		return $this->id;
	}

	private function resultQuery($result) {
		$list = null;
		$i = 0;
		while ( $dados = odbc_fetch_array ( $result ) ) {
			foreach ( $dados as $key => $value ) {
				if ($value !== '' and $value != null ) {
					$list [$key] [$i] = preg_replace('/[ ]{2,}|[\t]/', '', trim(iconv("ISO-8859-1", "UTF-8", $value)));
				}
			}
			$i ++;
		}
		return $list;
	}

	public function select($sql) : ?array{

		$this->connection ();
		$result = @odbc_exec (  $this->id, iconv("UTF-8" , "ISO-8859-1//IGNORE", $sql));

		if(!$result) return null;

		if (odbc_num_rows ($result) > 0) {
			
			$arr = array();
			$arr = odbc_fetch_array( $result );
	
			array_walk( $arr, function (&$entry) { $entry = iconv("ISO-8859-1", "UTF-8", $entry);});
			
			$this->closeConnection ();			
			return  $arr;
		}
	}

	public function selectList($sql) : ?array{
		
		$this->connection ();
		$result = @odbc_exec (  $this->id, iconv("UTF-8" , "ISO-8859-1//IGNORE", $sql));

		if(!$result) return null;

		if (odbc_num_rows ($result) > 0) {
			$result = $this->resultQuery ( $result );
			$this->closeConnection ();
			return $result;
		}
	}

	public function insert($data, $table, $pk) : int {

		$sql = $this->makeQueryInsert ( $data, $table, $pk );

		$this->connection ();

		// Query execute.
		$result = @odbc_exec (  $this->id, iconv("UTF-8" , "ISO-8859-1//IGNORE", $sql));

		if(!$result) return 0;

		odbc_free_result($result);
		
		$newResult = @odbc_exec (  $this->id,  "select @@identity");
		$lastInserId = odbc_result($newResult, 1);
		odbc_free_result($newResult);

		$this->closeConnection ();
		
		return $lastInserId;
	}

	public function update($data, $table, $pk = null) : bool {
		
		if(empty($data)) return false;
		if(empty($table)) return false;

		$this->connection ();

		$sql = $this->makeQueryUpdate ( $data, $table, $pk );
		$result = @odbc_exec (  $this->id, iconv("UTF-8" , "ISO-8859-1//IGNORE", $sql));

		if(!$result) return false;
	}

	public function delete($value, $table, $column) : bool {
		
		if(empty($table)) return false;
		if(empty($pk)) return false;
		if(empty($column)) return false;

		$this->connection ();

		$sql = " DELETE FROM $table WHERE $column = '$value' ";

		$result = @odbc_exec (  $this->id, iconv("UTF-8" , "ISO-8859-1//IGNORE", $sql));

		return ($result)? true: false;
	}

	private function makeQueryInsert($data, $table, $pk) : ?string{

		if(empty($data)) return false;
		if(empty($table)) return false;
		if(empty($pk)) return false;
		
		$i = 0;
		foreach ( $data as $key => $value ) {
			if (! empty ( $key ) and ( ($value !== '') and ($value !== null) )) {
				if ($pk != $key) {
					$string_columns [$i] = $key;
					$string_value [$i] = "'".trim(str_replace("'","''",$value))."'";
				}
			}
			$i ++;
		}
		
		$string_value = implode ( ',', $string_value );
		$string_columns = implode ( ',', $string_columns );
		$query = "insert into " . $table . " (" . $string_columns . ") values (" . $string_value . ")";

		return $query;
	}

	private function makeQueryUpdate($data, $table, $pk): ?string{
		$condition = null;
		$i = 0;
		foreach ( $data as $key => $value ) {
			if ($pk == $key and ! empty ( $key ) and ($value !== '') and ($value !== null) ) {
				$value = addslashes($value);
				$condition = $key . " = '" . $value . "'";
			} else {
				if(($value !== '') and ($value !== null) ){
					if($value === "0000-00-00"){
						$string_update [$i] = $key . " = NULL ";
					}else{
						$string_update [$i] = $key . " = '".trim((str_replace("'","''",$value)))."'";
					}
				}
			}
			$i ++;
		}
		
		$string_update = implode ( ',', $string_update );
		if(empty($condition)){
			return null;
		}
		
		return "update " . $table . " set " . $string_update . " where " . $condition;
	}
}
?>
