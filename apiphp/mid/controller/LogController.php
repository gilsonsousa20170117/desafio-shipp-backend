<?php

namespace controller;

use cnn\ConnectionMssql;
use dao\LogDao;

class LogController {

    private $objLog;
    private $arr;
    private $url;

    function __construct($url){

        $this->url = $url;
        $this->arr = $_GET;

        $this->objLog = new LogDao();
        $this->check();
        $this->callApi();
    }

    public function callApi(){
        
        $long = $this->arr["long"];
        $lat = $this->arr["lat"];

        $url = $this->url."?long=".$long."&lat=".$lat;

        $result  = file_get_contents($url);
        $statusCode = $http_response_header[0];

        if(!$result){
            http_response_code(400);
            exit();
        }
          
        $this->save(sizeof(json_decode($result,true)["county"]),$statusCode);

        header('Content-Type: application/json');
        echo $result;
        exit();
    }

    public function check(){
        if(empty($this->arr["long"])){
            http_response_code(400);
            exit();
        }
        if(empty($this->arr["lat"])){
            http_response_code(400);
            exit();
        }
    }
    
    public function save($total, $statusCode){

        $this->objLog->setIp($this->getIp());
        $this->objLog->setRequest($this->getRequest());
        $this->objLog->setBrowser($this->getBrowser());
        
        $this->objLog->setLogitude($this->arr["long"]);
        $this->objLog->setLatitude($this->arr["lat"]);

        $this->objLog->setTotal($total);
        $this->objLog->setStatusCode($statusCode);

        $this->objLog->setSaveData(date("Y-m-d H:i:s"));

        $this->objLog->create();
    }

    private function getRequest(){
        return !empty($_SERVER["REQUEST_URI"])? $_SERVER["REQUEST_URI"]:null;
    }

    private function getIp():string{ 
        if (!empty($_SERVER['HTTP_CLIENT_IP'])){
			return $_SERVER['HTTP_CLIENT_IP'];
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){	
			return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
		return $_SERVER['REMOTE_ADDR'];
    }

    private function getBrowser(): ?string {

        if(empty($_SERVER['HTTP_USER_AGENT'])) return null;
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        
		if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
			return 'IE - '.$matched[1] ;
        } 
        
        if (preg_match( '|Opera/([0-9].[0-9]{1,2})|',$useragent,$matched)) {
			return 'Opera - '.$matched[1] ;
        } 
        
        if(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
			return 'Firefox - '.$matched[1] ;
        } 
        
        if(preg_match('|Chrome/([0-9\.]+)|',$useragent,$matched)) {
			return 'Chrome - '.$matched[1] ;
        } 
        
        if(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
			
			return 'Safari - '.$matched[1] ;
        } 
        
		return 'other - 0';
    }

}

