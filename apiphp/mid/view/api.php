<?php

require_once "../config/config.php";
require_once "LogController.php";


use controller\LogController;


// http://localhost/apiphp/mid/view/V1/stores?long=-73.70053900&lat=42.75442400

$req = !empty($_SERVER["REDIRECT_URL"])? $_SERVER["REDIRECT_URL"]:null;

if($req == "/apiphp/mid/view/V1/stores"){
	new LogController("http://localhost/apiphp/api/view/V1/stores");
}else{
	http_response_code(400);
	exit();
}

