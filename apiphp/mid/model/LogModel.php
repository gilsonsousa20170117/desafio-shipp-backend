<?php

namespace model;

class LogModel {
    
	private $arr;

	function setLog($value){ $this->arr = $value; }
    function getLog(){ return $this->arr; }

    function setId($value){ $this->arr["ST08_ID"] = !empty($value)? $value:null; }
    function getId(){ return !empty($this->arr["ST08_ID"])? $this->arr["ST08_ID"]:null; }
   
    function setRequest($value){ $this->arr["ST08_DS_REQUEST"] = !empty($value)? $value:null; }
    function getRequest(){ return !empty($this->arr["ST08_DS_REQUEST"])? $this->arr["ST08_DS_REQUEST"]:null; }

    function setIp($value){ $this->arr["ST08_DS_IP"] = !empty($value)? $value:null; }
    function getIp(){ return !empty($this->arr["ST08_DS_IP"])? $this->arr["ST08_DS_IP"]:null; }

    function setBrowser($value){ $this->arr["ST08_NM_BROWSER"] = !empty($value)? $value:null; }
    function getBrowser(){ return !empty($this->arr["ST08_NM_BROWSER"])? $this->arr["ST08_NM_BROWSER"]:null; }

    function setLogitude($value){ $this->arr["ST08_CD_LOGITUDE"] = !empty($value)? $value:null; }
    function getLogitude(){ return !empty($this->arr["ST08_CD_LOGITUDE"])? $this->arr["ST08_CD_LOGITUDE"]:null; }

    function setLatitude($value){ $this->arr["ST08_CD_LATITUDE"] = !empty($value)? $value:null; }
    function getLatitude(){ return !empty($this->arr["ST08_CD_LATITUDE"])? $this->arr["ST08_CD_LATITUDE"]:null; }
    
    function setTotal($value){ $this->arr["ST08_NR_TOTAL"] = !empty($value)? $value:null; }
    function getTotal(){ return !empty($this->arr["ST08_NR_TOTAL"])? $this->arr["ST08_NR_TOTAL"]:null; }

    function setStatusCode($value){ $this->arr["ST08_NM_STATUS_CODE"] = !empty($value)? $value:null; }
    function getStatusCode(){ return !empty($this->arr["ST08_NM_STATUS_CODE"])? $this->arr["ST08_NM_STATUS_CODE"]:null; }
    
    function setSaveData($value){ $this->arr["ST08_DT_SAVE"] = !empty($value)? $value:null; }
    function getSaveData(){ return !empty($this->arr["ST08_DT_SAVE"])? $this->arr["ST08_DT_SAVE"]:null; }
}