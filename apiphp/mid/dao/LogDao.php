<?php

namespace dao;

use cnn\ConnectionMssql;
use model\LogModel;

class LogDao extends LogModel {

	private $cnn;

	function __construct(){
		$this->cnn = new ConnectionMssql();
	 }

	// INSERT

	function create(){
		return $this->cnn->insert($this->getLog(),"ST08_LOG","ST08_ID");
	}

	// UPDATE

	function update(){
		return $this->cnn->update($this->getLog(),"ST08_LOG","ST08_ID");
	}

	// DELETE

	function delete($id){
		return $this->cnn->delete("ST08_LOG","ST08_ID", $id );
	}

	// SELECT

	function readOne($id){
		 
        $result = $this->cnn->select("select * from ST08_LOG where ST08_ID = '$id' ");
         
		$this->setLog($result);
		return ($result)?true:false;
	}

	function readAll(){
         
        $result = $this->cnn->selectList("select * from ST06_STATE ");
         
		$this->setLog($result);
		return ($result)?true:false;
	}

	function readIn($idArr){
		$i = 0;
		$data = array();
		foreach($idArr as $id){
			$result = $this->cnn->select("select * from ST08_LOG where ST08_ID = '$id' ");
			$keys = array_keys($result);
			foreach ( $keys as $key ) {
				$data[$key][$i] = $result[$key];
			}
			$i++;
		}
		$this->setLog($data);
		return ($result)?true:false;
	}
}
