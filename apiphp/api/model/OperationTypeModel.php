<?php

namespace model;

class OperationTypeModel {
    
	private $arr;

	function setOperationType($value){ $this->arr = $value; }
    function getOperationType(){ return $this->arr; }

    function setId($value){ $this->arr["ST04_ID"] = !empty($value)? $value:null; }
    function getId(){ return !empty($this->arr["ST04_ID"])? $this->arr["ST04_ID"]:null; }
   
    function setName($value){ $this->arr["ST04_NM_NAME"] = !empty($value)? $value:null; }
    function getName(){ return !empty($this->arr["ST04_NM_NAME"])? $this->arr["ST04_NM_NAME"]:null; }

}