<?php

namespace model;

class CityModel {
    
	private $arr;

	function setCity($value){ $this->arr = $value; }
    function getCity(){ return $this->arr; }

    // PK

    function setId($value){ $this->arr["ST07_ID"] = !empty($value)? $value:null; }
    function getId(){ return !empty($this->arr["ST07_ID"])? $this->arr["ST07_ID"]:null; }
    
    // FK

    function setStateId($value){ $this->arr["ST07_ST06_ID"] = !empty($value)? $value:null; }
    function getStateId(){ return !empty($this->arr["ST07_ST06_ID"])? $this->arr["ST07_ST06_ID"]:null; }
   
    // Others

    function setName($value){ $this->arr["ST07_NM_NAME"] = !empty($value)? $value:null; }
    function getName(){ return !empty($this->arr["ST07_NM_NAME"])? $this->arr["ST07_NM_NAME"]:null; }

}