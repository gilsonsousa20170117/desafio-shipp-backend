<?php

namespace model;

class AddressModel {
    
	private $arr;

    // All data
	function setAddress($value){ $this->arr = $value; }
    function getAddress(){ return $this->arr; }

    // Pk
    function setId($value){ $this->arr["ST01_ID"] = !empty($value)? $value:null; }
    function getId(){ return !empty($this->arr["ST01_ID"])? $this->arr["ST01_ID"]:null; }

    // FK
    function setEntityId($value){ $this->arr["ST02_ST01_ID"] = !empty($value)? $value:null; }
    function getEntityId(){ return !empty($this->arr["ST02_ST01_ID"])? $this->arr["ST02_ST01_ID"]:null; }

    function setCountyId($value){ $this->arr["ST02_ST03_ID"] = !empty($value)? $value:null; }
    function getCountyId(){ return !empty($this->arr["ST02_ST03_ID"])? $this->arr["ST02_ST03_ID"]:null; }
    
    function setOperationTypeId($value){ $this->arr["ST02_ST04_ID"] = !empty($value)? $value:null; }
    function getOperationTypeId(){ return !empty($this->arr["ST02_ST04_ID"])? $this->arr["ST02_ST04_ID"]:null; }

    function setEstablishmentTypeId($value){ $this->arr["ST02_ST05_ID"] = !empty($value)? $value:null; }
    function getEstablishmentTypeId(){ return !empty($this->arr["ST02_ST05_ID"])? $this->arr["ST02_ST05_ID"]:null; }

    function setCityId($value){ $this->arr["ST02_ST07_ID"] = !empty($value)? $value:null; }
    function getCityId(){ return !empty($this->arr["ST02_ST07_ID"])? $this->arr["ST02_ST07_ID"]:null; }

    // Others datas.
    function setCodigoLicense($value){ $this->arr["ST02_CD_LICENSE"] = !empty($value)? $value:null; }
    function getCodigoLicense(){ return !empty($this->arr["ST02_CD_LICENSE"])? $this->arr["ST02_CD_LICENSE"]:null; }
    
    function setDatabase($value){ $this->arr["ST02_MN_DB"] = !empty($value)? $value:null; }
    function getDatabase(){ return !empty($this->arr["ST02_MN_DB"])? $this->arr["ST02_MN_DB"]:null; }

    function setStreetNumber($value){ $this->arr["ST02_NR_STREET"] = !empty($value)? $value:null; }
    function getStreetNumber(){ return !empty($this->arr["ST02_NR_STREET"])? $this->arr["ST02_NR_STREET"]:null; }

    function setStreet($value){ $this->arr["ST02_MN_STREET"] = !empty($value)? $value:null; }
    function getStreet(){ return !empty($this->arr["ST02_MN_STREET"])? $this->arr["ST02_MN_STREET"]:null; }

    function setAddressFirst($value){ $this->arr["ST02_MN_ADDRESS"] = !empty($value)? $value:null; }
    function getAddressFirst(){ return !empty($this->arr["ST02_MN_ADDRESS"])? $this->arr["ST02_MN_ADDRESS"]:null; }

    function setAddressSec($value){ $this->arr["ST02_MN_ADDRESS2"] = !empty($value)? $value:null; }
    function getAddressSec(){ return !empty($this->arr["ST02_MN_ADDRESS2"])? $this->arr["ST02_MN_ADDRESS2"]:null; }

    function setZipCode($value){ $this->arr["ST02_CD_ZIP"] = !empty($value)? $value:null; }
    function getZipCode(){ return !empty($this->arr["ST02_CD_ZIP"])? $this->arr["ST02_CD_ZIP"]:null; }

    function setSquareFootage($value){ $this->arr["ST02_NR_SQUARE_FOOTAGE"] = !empty($value)? $value:null; }
    function getSquereFootage(){ return !empty($this->arr["ST02_NR_SQUARE_FOOTAGE"])? $this->arr["ST02_NR_SQUARE_FOOTAGE"]:null; }

    function setLogitude($value){ $this->arr["ST02_CD_LOGITUDE"] = !empty($value)? $value:null; }
    function getLogitude(){ return !empty($this->arr["ST02_CD_LOGITUDE"])? $this->arr["ST02_CD_LOGITUDE"]:null; }

    function setLatitude($value){ $this->arr["ST02_CD_LATITUDE"] = !empty($value)? $value:null; }
    function getLatitude(){ return !empty($this->arr["ST02_CD_LATITUDE"])? $this->arr["ST02_CD_LATITUDE"]:null; }

    function setTempRange($value){ $this->arr["TMP_RANGE"] = !empty($value)? $value:null; }
    function getTempRange(){ return !empty($this->arr["TMP_RANGE"])? $this->arr["TMP_RAGEM"]:null; }

}