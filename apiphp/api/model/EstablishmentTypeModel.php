<?php

namespace model;

class EstablishmentTypeModel {
    
	private $arr;

	function setEstablishmentType($value){ $this->arr = $value; }
    function getEstablishmentType(){ return $this->arr; }

    function setId($value){ $this->arr["ST05_ID"] = !empty($value)? $value:null; }
    function getId(){ return !empty($this->arr["ST05_ID"])? $this->arr["ST05_ID"]:null; }
   
    function setName($value){ $this->arr["ST05_NM_NAME"] = !empty($value)? $value:null; }
    function getName(){ return !empty($this->arr["ST05_NM_NAME"])? $this->arr["ST05_NM_NAME"]:null; }

}