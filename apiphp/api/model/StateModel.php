<?php

namespace model;

class StateModel {
    
	private $arr;

	function setState($value){ $this->arr = $value; }
    function getState(){ return $this->arr; }

    function setId($value){ $this->arr["ST06_ID"] = !empty($value)? $value:null; }
    function getId(){ return !empty($this->arr["ST06_ID"])? $this->arr["ST06_ID"]:null; }
   
    function setName($value){ $this->arr["ST06_SG_NAME"] = !empty($value)? $value:null; }
    function getName(){ return !empty($this->arr["ST06_SG_NAME"])? $this->arr["ST06_SG_NAME"]:null; }

}