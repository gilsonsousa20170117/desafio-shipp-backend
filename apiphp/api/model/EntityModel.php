<?php

namespace model;

class EntityModel {
    
	private $arr;

	function setEntity($value){ $this->arr = $value; }
    function getEntity(){ return $this->arr; }

    function setId($value){ $this->arr["ST01_ID"] = !empty($value)? $value:null; }
    function getId(){ return !empty($this->arr["ST01_ID"])? $this->arr["ST01_ID"]:null; }
   
    function setName($value){ $this->arr["ST01_NM_NAME"] = !empty($value)? $value:null; }
    function getName(){ return !empty($this->arr["ST01_NM_NAME"])? $this->arr["ST01_NM_NAME"]:null; }

}