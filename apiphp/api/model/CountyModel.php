<?php

namespace model;

class CountyModel {
    
	private $arr;

	function setCounty($value){ $this->arr = $value; }
    function getCounty(){ return $this->arr; }

    function setId($value){ $this->arr["ST03_ID"] = !empty($value)? $value:null; }
    function getId(){ return !empty($this->arr["ST03_ID"])? $this->arr["ST03_ID"]:null; }
   
    function setName($value){ $this->arr["ST03_NM_NAME"] = !empty($value)? $value:null; }
    function getName(){ return !empty($this->arr["ST03_NM_NAME"])? $this->arr["ST03_NM_NAME"]:null; }

}