<?php

namespace controller;

use cnn\ConnectionMssql;

use dao\EntityDao;
use dao\AddressDao;
use dao\CountyDao;
use dao\OperationTypeDao;
use dao\EstablimentTypeDao;
use dao\StateDao;
use dao\CityDao;

class ApiController {

	private $objAddress;
	private $objEntity;
	private $objCounty;
	private $objOperationType;
	private $objEstablishmentType;
	private $objCity;
	private $objState;
	
	function  __construct($arr){

		$this->objAddress = new AddressDao();
		$this->objEntity = new EntityDao();
		$this->objCounty = new CountyDao();
		$this->objOperationType = new OperationTypeDao();
		$this->objEstablishmentType = new EstablimentTypeDao();
		$this->objCity = new CityDao();
		$this->objState = new StateDao();

		$this->search($arr["long"],$arr["lat"]);
	}
	    
	public function search($long ,$lat){

		// Make search
		$this->objAddress->readyNearPosition($long, $lat);
		
		// Get Entity
		$this->objEntity->readIn($this->objAddress->getEntityId());

		// Get County
		$this->objCounty->readIn($this->objAddress->getCountyId());	

		// Get Operation Type
		$this->objOperationType->readIn($this->objAddress->getOperationTypeId());

		// Get Establishment Type
		$this->objEstablishmentType->readIn($this->objAddress->getEstablishmentTypeId());

		// Get City
		$this->objCity->readIn($this->objAddress->getCityId());
		
		// Get State
		$this->objState->readIn($this->objCity->getStateId());

		// Create json return
		$data["county"] = $this->objCounty->getName();
		$data["entity"] = $this->objEntity->getName();
		$data["operation_type"] = $this->objOperationType->getName();
		$data["establishment_type"] = $this->objEstablishmentType->getName();
		$data["city"] = $this->objCity->getName();
		$data["state"] = $this->objState->getName();
		
		$data["street_number"] = $this->objAddress->getStreetNumber();
		$data["street"] = $this->objAddress->getStreet();
		$data["zip_code"] = $this->objAddress->getZipCode();
		$data["longitutde"] = $this->objAddress->getLogitude();
		$data["latitude"] = $this->objAddress->getLatitude();
		
		header('Content-Type: application/json');
		echo json_encode($data);
		exit;
	}


}

?>