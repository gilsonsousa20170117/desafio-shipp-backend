<?php

require_once "../config/config.php";
require_once "ApiController.php";

use controller\ApiController;

// http://localhost/apiphp/api/view/V1/stores?long=-73.70053900&lat=42.75442400

$req = !empty($_SERVER["REDIRECT_URL"])? $_SERVER["REDIRECT_URL"]:null;

if($req == "/apiphp/api/view/V1/stores"){
	new ApiController($_GET);
}else{
	http_response_code(400);
	die();
}
