<?php

//
//
// PATHS

set_include_path (
"../controller" . PATH_SEPARATOR . 
"../cnn" . PATH_SEPARATOR . 
"../dao" . PATH_SEPARATOR . 
"../model" . PATH_SEPARATOR 
);

//
//
// Data base config.

date_default_timezone_set ( 'America/Sao_Paulo' );

define ( "DB_URL"   , "DESKTOP-53SG5SQ\SQLEXPRESS" );
define ( "DB_NAME"  , "DB_STORES");
define ( "DB_USER"  , "gsu" );
define ( "DB_PWD"   , "gsuchoa4" );

// Database Connection
require_once "ConnectionMssql.php";

// Model
require_once "AddressModel.php";
require_once "CountyModel.php";
require_once "EntityModel.php";
require_once "EstablishmentTypeModel.php";
require_once "OperationTypeModel.php";
require_once "StateModel.php";
require_once "CityModel.php";

// Dao
require_once "AddressDao.php";
require_once "CountyDao.php";
require_once "EntityDao.php";
require_once "EstablishmentTypeDao.php";
require_once "OperationTypeDao.php";
require_once "StateDao.php";
require_once "CityDao.php";
?>
