<?php

namespace dao;

use cnn\ConnectionMssql;
use model\EntityModel;

class EntityDao extends EntityModel {

	private $cnn;

	function __construct(){
		$this->cnn = new ConnectionMssql();
	}

	// INSERT

	function create(){
		return $this->cnn->insert($this->getEntity(),"ST01_ENTITY","ST01_ID");
	}

	// UPDATE

	function update(){
		return $this->cnn->update($this->getEntity(),"ST01_ENTITY","ST01_ID");
	}

	// DELETE

	function delete($id){
		return $this->cnn->delete("ST01_ENTITY","ST01_ID", $id );
	}

	// SELECT

	function readOne($id){
		 
        $result = $this->cnn->select("select * from ST01_ENTITY where ST01_ID = '$id' ");
         
		$this->setEntity($result);
		return ($result)?true:false;
	 }

	function readAll(){
         
        $result = $this->cnn->selectList("select * from ST01_ENTITY ");
         
		$this->setEntity($result);
		return ($result)?true:false;
	}

	function readIn($idArr){
		$i = 0;
		$data = array();
		foreach($idArr as $id){
			$result = $this->cnn->select("select * from ST01_ENTITY where ST01_ID = '$id' ");
			$keys = array_keys($result);
			foreach ( $keys as $key ) {
				$data[$key][$i] = $result[$key];
			}
			$i++;
		}
		$this->setEntity($data);
		return ($result)?true:false;
	}
}
