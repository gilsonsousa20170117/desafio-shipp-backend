<?php

namespace dao;

use cnn\ConnectionMssql;
use model\CityModel;

class CityDao extends CityModel {

	private $cnn;

	function __construct(){
		$this->cnn = new ConnectionMssql();
	}

	// INSERT

	function create(){
		return $this->cnn->insert($this->getCity(),"ST07_CITY","ST07_ID");
	}

	// UPDATE

	function update(){
		return $this->cnn->update($this->getCity(),"ST07_CITY","ST07_ID");
	}

	// DELETE

	function delete($id){
		return $this->cnn->delete("ST07_CITY","ST07_ID", $id );
	}

	// SELECT

	function readOne($id){
		 
        $result = $this->cnn->select("select * from ST07_CITY where ST07_ID = '$id' ");
         
		$this->setCity($result);
		return ($result)?true:false;
	}

	function readAll(){
         
        $result = $this->cnn->selectList("select * from ST07_CITY ");
         
		$this->setCity($result);
		return ($result)?true:false;
	}

	function readIn($idArr){
		$i = 0;
		$data = array();
		foreach($idArr as $id){
			$result = $this->cnn->select("select * from ST07_CITY where ST07_ID = '$id' ");
			$keys = array_keys($result);
			foreach ( $keys as $key ) {
				$data[$key][$i] = $result[$key];
			}
			$i++;
		}
		$this->setCity($data);
		return ($result)?true:false;
	}
}
