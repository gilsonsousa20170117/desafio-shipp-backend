<?php

namespace dao;

use cnn\ConnectionMssql;
use model\StateModel;

class StateDao extends StateModel {

	private $cnn;

	function __construct(){
		$this->cnn = new ConnectionMssql();
	 }

	// INSERT

	function create(){
		return $this->cnn->insert($this->getState(),"ST06_STATE","ST06_ID");
	}

	// UPDATE

	function update(){
		return $this->cnn->update($this->getState(),"ST06_STATE","ST06_ID");
	}

	// DELETE

	function delete($id){
		return $this->cnn->delete("ST06_STATE","ST06_ID", $id );
	}

	// SELECT

	function readOne($id){
		 
        $result = $this->cnn->select("select * from ST06_STATE where ST06_ID = '$id' ");
         
		$this->setState($result);
		return ($result)?true:false;
	}

	function readAll(){
         
        $result = $this->cnn->selectList("select * from ST06_STATE ");
         
		$this->setState($result);
		return ($result)?true:false;
	}

	function readIn($idArr){
		$i = 0;
		$data = array();
		foreach($idArr as $id){
			$result = $this->cnn->select("select * from ST06_STATE where ST06_ID = '$id' ");
			$keys = array_keys($result);
			foreach ( $keys as $key ) {
				$data[$key][$i] = $result[$key];
			}
			$i++;
		}
		$this->setState($data);
		return ($result)?true:false;
	}
}
