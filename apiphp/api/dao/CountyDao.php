<?php

namespace dao;

use cnn\ConnectionMssql;
use model\CountyModel;

class CountyDao extends CountyModel {

	private $cnn;

	function __construct(){
		$this->cnn = new ConnectionMssql();
	}

	// INSERT

	function create(){
		return $this->cnn->insert($this->getCounty(),"ST03_COUNTY","ST03_ID");
	}

	// UPDATE

	function update(){
		return $this->cnn->update($this->getCounty(),"ST03_COUNTY","ST03_ID");
	}

	// DELETE

	function delete($id){
		return $this->cnn->delete("ST03_COUNTY","ST03_ID", $id );
	}

	// SELECT

	function readOne($id){
		 
        $result = $this->cnn->select("select * from ST03_COUNTY where ST03_ID = '$id' ");
         
		$this->setCounty($result);
		return ($result)?true:false;
	 }

	function readAll(){
         
        $result = $this->cnn->selectList("select * from ST03_COUNTY ");
         
		$this->setCounty($result);
		return ($result)?true:false;
	}
	
	function readIn($idArr){
		$i = 0;
		$data = array();
		foreach($idArr as $id){
			$result = $this->cnn->select("select * from ST03_COUNTY where ST03_ID = '$id' ");
			$keys = array_keys($result);
			foreach ( $keys as $key ) {
				$data[$key][$i] = $result[$key];
			}
			$i++;
		}
		$this->setCounty($data);
		return ($result)?true:false;
	}
}
