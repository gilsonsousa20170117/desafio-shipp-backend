<?php

namespace dao;

use cnn\ConnectionMssql;
use model\EstablishmentTypeModel;

class EstablimentTypeDao extends EstablishmentTypeModel {

	private $cnn;

	function __construct(){
		$this->cnn = new ConnectionMssql();
	}

	// INSERT

	function create(){
		return $this->cnn->insert($this->getEstablishmentType(),"ST05_ESTABLISHMENT_TYPE","ST05_ID");
	}

	// UPDATE

	function update(){
		return $this->cnn->update($this->getEstablishmentType(),"ST05_ESTABLISHMENT_TYPE","ST05_ID");
	}

	// DELETE

	function delete($id){
		return $this->cnn->delete("ST05_ESTABLISHMENT_TYPE","ST05_ID", $id );
	}

	// SELECT

	function readOne($id){
		 
        $result = $this->cnn->select("select * from ST05_ESTABLISHMENT_TYPE where ST05_ID = '$id' ");
         
		$this->setEstablishmentType($result);
		return ($result)?true:false;
	}

	function readAll(){
         
        $result = $this->cnn->selectList("select * from ST05_ESTABLISHMENT_TYPE ");
         
		$this->setEstablishmentType($result);
		return ($result)?true:false;
	}

	function readIn($idArr){
		$i = 0;
		$data = array();
		foreach($idArr as $id){
			$result = $this->cnn->select("select * from ST05_ESTABLISHMENT_TYPE where ST05_ID = '$id' ");
			$keys = array_keys($result);
			foreach ( $keys as $key ) {
				$data[$key][$i] = $result[$key];
			}
			$i++;
		}
		$this->setEstablishmentType($data);
		return ($result)?true:false;
	}
}
