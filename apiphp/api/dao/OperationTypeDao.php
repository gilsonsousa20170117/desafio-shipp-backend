<?php

namespace dao;

use cnn\ConnectionMssql;
use model\OperationTypeModel;

class OperationTypeDao extends OperationTypeModel {

	private $cnn;

	function __construct(){
		$this->cnn = new ConnectionMssql();
	}

	// INSERT

	function create(){
		return $this->cnn->insert($this->getOperationType(),"ST04_OPERATION_TYPE","ST04_ID");
	}

	// UPDATE

	function update(){
		return $this->cnn->update($this->getOperationType(),"ST04_OPERATION_TYPE","ST04_ID");
	}

	// DELETE

	function delete($id){
		return $this->cnn->delete("ST04_OPERATION_TYPE","ST04_ID", $id );
	}

	// SELECT

	function readOne($id){
		 
        $result = $this->cnn->select("select * from ST04_OPERATION_TYPE where ST04_ID = '$id' ");
         
		$this->setOperationType($result);
		return ($result)?true:false;
	}

	function readAll(){
         
        $result = $this->cnn->selectList("select * from ST04_OPERATION_TYPE ");
         
		$this->setOperationType($result);
		return ($result)?true:false;
	}

	function readIn($idArr){
		$i = 0;
		$data = array();
		foreach($idArr as $id){
			$result = $this->cnn->select("select * from ST04_OPERATION_TYPE where ST04_ID = '$id' ");
			$keys = array_keys($result);
			foreach ( $keys as $key ) {
				$data[$key][$i] = $result[$key];
			}
			$i++;
		}
		$this->setOperationType($data);
		return ($result)?true:false;
	}
}
