<?php

namespace dao;

use cnn\ConnectionMssql;
use model\AddressModel;

class AddressDao extends AddressModel {

	private $cnn;

	function __construct(){
		$this->cnn = new ConnectionMssql();
	}

	// INSERT

	function create(){
		return $this->cnn->insert($this->getAddress(),"ST02_ADDRESS","ST02_ID");
	}

	// UPDATE

	function update(){
		return $this->cnn->update($this->getAddress(),"ST02_ADDRESS","ST02_ID");
	}

	// DELETE

	function delete($id){
		return $this->cnn->delete("ST02_ADDRESS","ST02_ID", $id );
	}

	// SELECT

	function readOne($id){
		 
         $result = $this->cnn->select("select * from ST02_ADDRESS where ST02_ID = '$id' ");
         
		 $this->setAddress($result);
		 return ($result)?true:false;
	}

	function readAll(){
         
        $result = $this->cnn->selectList("select * from ST02_ADDRESS ");
         
		$this->setAddress($result);
		return ($result)?true:false;
	}

	function readIn($idArr){
		$i = 0;
		$data = array();
		foreach($idArr as $id){
			$result = $this->cnn->select("select * from ST02_ADDRESS where ST02_ID = '$id' ");
			$keys = array_keys($result);
			foreach ( $keys as $key ) {
				$data[$key][$i] = $result[$key];
			}
			$i++;
		}
		$this->setAddress($data);
		return ($result)?true:false;
	}

	function readyNearPosition($long, $lat){
		$sql = "SELECT 
				*
				FROM (
					SELECT   ST02_ID
							,ST02_ST01_ID
							,ST02_ST03_ID
							,ST02_ST04_ID
							,ST02_ST05_ID
							,ST02_ST07_ID
							,ST02_CD_LICENSE
							,ST02_MN_DB
							,ST02_NR_STREET
							,ST02_MN_STREET
							,ST02_MN_ADDRESS
							,ST02_MN_ADDRESS2
							,ST02_CD_ZIP
							,ST02_NR_SQUARE_FOOTAGE
							,ST02_CD_LOGITUDE
							,ST02_CD_LATITUDE
							
							,TMP_RANGE = (6371 *
								acos(
									cos(radians($lat)) *
									cos(radians(ST02_CD_LATITUDE)
								) *
								cos(radians($long) - radians(ST02_CD_LOGITUDE)) + 
								sin(radians($lat) ) * sin(radians(ST02_CD_LATITUDE))
							))
							
							
					FROM ST02_ADDRESS 
				) AS X
				WHERE TMP_RANGE <= 6.5 
				ORDER BY TMP_RANGE";

		$result = $this->cnn->selectList($sql);
         
		$this->setAddress($result);
		return ($result)?true:false;
	}
}
