#!/usr/bin/python
# -*- coding: utf-8 -*-
import pyodbc

class ConnectionMssql:
	conn = None
	db = None
	
	def __init__( self, url, base, user, pwd):
		self.conn = pyodbc.connect('DRIVER={SQL Server Native Client 11.0};SERVER='+url+';DATABASE='+base+';UID='+user+';PWD='+pwd)
		self.db = self.conn.cursor()
		
	def select(self, sql):
		self.db.execute(sql)
		recordset = self.db.fetchall()
		if not recordset:
			return None
		else:
			return recordset

	def insert(self, sql):
		self.db.execute(sql)
		self.conn.commit()
