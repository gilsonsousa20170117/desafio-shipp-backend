
SELECT 
*
FROM (
	SELECT   ST02_ID
			,ST02_ST01_ID
			,ST02_ST03_ID
			,ST02_ST04_ID
			,ST02_ST05_ID
			,ST02_ST07_ID
			,ST02_CD_LICENSE
			,ST02_MN_DB
			,ST02_NR_STREET
			,ST02_MN_STREET
			,ST02_MN_ADDRESS
			,ST02_MN_ADDRESS2
			,ST02_CD_ZIP
			,ST02_NR_SQUARE_FOOTAGE
			,ST02_CD_LOGITUDE
			,ST02_CD_LATITUDE
			,distancia = (6371 *
				acos(
					cos(radians(42.75442400)) *
					cos(radians(ST02_CD_LATITUDE)
				) *
				cos(radians(-73.70053900) - radians(ST02_CD_LOGITUDE)) + sin(radians(42.75442400) ) * sin(radians(ST02_CD_LATITUDE))
			))
			,dist2 = ( 6371 * acos( cos( radians(42.75442400) ) 
              * cos( radians( ST02_CD_LATITUDE ) ) 
              * cos( radians( ST02_CD_LOGITUDE ) - radians(-73.70053900) ) 
              + sin( radians(42.75442400) ) 
              * sin( radians( ST02_CD_LATITUDE ) ) ) )
			
	FROM ST02_ADDRESS 
) AS X
WHERE distancia <= 6.5
