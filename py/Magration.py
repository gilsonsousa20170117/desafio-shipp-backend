#!/usr/bin/python
# -*- coding: utf-8 -*-

import csv
import time
import json as JSON

from ConnectionMssql import ConnectionMssql

# Código para teste

class Migration:
    
    cnn = None

    def __init__(self):
        
        # Make database connnection.
        self.cnn = ConnectionMssql('DESKTOP-53SG5SQ\\SQLEXPRESS', 'DB_STORES', 'gsu', 'gsuchoa4')
        #print (self.cnn)

        # Open up data source;
        csvFile = "C:/wamp64/www/TestApiRest/desafio-shipp-backend/py/stores.csv"

        print(" Iniciando insert dos dados auxiliares!")
        time.sleep(1)
        self.saveOthers(csvFile)

        print(" Iniciando insert dos dados das cidades e gerando o relacionamento com estado!")
        time.sleep(1)
        self.saveCity(csvFile)

        print(" Iniciando insert dos dados do endereço e gerando todos os relacionamentos!")
        time.sleep(1)
        self.saveAddress(csvFile)

    def saveOthers(self, fl):
        cf = open(fl)
        # Read data source.
        with cf as csvfile:
            rd = csv.reader(csvfile)
            # skip first row
            next(rd, None) 
            for row in rd:
                self.insertCounty(row[0])
                self.insertOperationType(row[2])
                self.insertEstablishmentType(row[3])
                self.insertEntity(row[4])
                self.insertState(row[11])
    
    def saveCity(self, fl):
        cf = open(fl)
        with cf as csvfile:
            rd = csv.reader(csvfile)
            next(rd, None)
            for row in rd:
                self.insertCity(row[11],row[10])

    def saveAddress(self, fl):
        cf = open(fl)
        with cf as csvfile:
            rd = csv.reader(csvfile)
            next(rd, None)
            for row in rd:
                self.insertAddress(row)

    def insertEntity(self, value):
        if value is None:
            return
        name = value.strip().replace("'", "''")
        result = self.selectEntity(name)
        if result is None:
            sqlInsert = "INSERT INTO DB_STORES.dbo.ST01_ENTITY (ST01_NM_NAME) VALUES ('"+name+"')"
            print(sqlInsert)
            self.cnn.insert(sqlInsert)
        
    def insertCounty(self,value):
        if value is None:
            return
        name = value.strip().replace("'", "''")
        result = self.selectCounty(name)
        if result is None:
            sqlInsert = "INSERT INTO [DB_STORES].[dbo].[ST03_COUNTY] (ST03_NM_NAME) VALUES ('"+name+"')"
            print(sqlInsert)
            self.cnn.insert(sqlInsert)
        
    def insertOperationType(self, value):
        if value is None:
            return
        name = value.strip().replace("'", "''")
        result = self.selectOperationType(name)
        if result is None:
            sqlInsert = "INSERT INTO [DB_STORES].[dbo].[ST04_OPERATION_TYPE] (ST04_NM_NAME) VALUES ('"+name+"')"
            print(sqlInsert)
            self.cnn.insert(sqlInsert)
        
    def insertEstablishmentType(self, value):
        if value is None:
            return
        name = value.strip().replace("'", "''")
        result = self.selectEstablishimentType(value)
        if result is None:
            sqlInsert = "INSERT INTO [DB_STORES].[dbo].[ST05_ESTABLISHMENT_TYPE] (ST05_NM_NAME) VALUES ('"+name+"')"
            print(sqlInsert)
            self.cnn.insert(sqlInsert)

    def insertState(self, value):
        if value is None:
            return
        name = value.strip().replace("'", "''")
        result = self.selectState(name)
        if result is None:
            sqlInsert = "INSERT INTO [DB_STORES].[dbo].[ST06_STATE] (ST06_SG_NAME) VALUES ('"+name+"')"
            print(sqlInsert)
            self.cnn.insert(sqlInsert)

    def insertCity(self, state, city):

        if state is None or city is None:
            return

        # State
        idState  = self.selectState(state.strip().replace("'", "''"))
        if idState is not None:

            # City
            value = city.strip().replace("'", "''")

            result2 = self.selectCity(idState, value)

            if result2 is None:
                sqlInsert = "INSERT INTO [DB_STORES].[dbo].[ST07_CITY] (ST07_ST06_ID, ST07_NM_NAME) VALUES ("+str(idState)+",'"+value+"')"
                print(sqlInsert)
                self.cnn.insert(sqlInsert)

    def insertAddress(self, row):

        if row is None:
            return

        # Relations 
        idEntity    = self.selectEntity(row[4].strip().replace("'", "''"))
        idCounty    = self.selectCounty(row[0].strip().replace("'", "''"))
        idType      = self.selectOperationType(row[2].strip().replace("'", "''"))
        idEstType   = self.selectEstablishimentType(row[3].strip().replace("'", "''"))
        idState     = self.selectState(row[11].strip().replace("'", "''"))
        idCity      = self.selectCity(idState, row[10].strip().replace("'", "''"))
        
        # Create query insert.
        sql = " INSERT INTO [DB_STORES].[dbo].[ST02_ADDRESS] ("

        sql = sql + " ST02_ST01_ID "
        sql = sql + " ,ST02_ST03_ID "
        sql = sql + " ,ST02_ST04_ID "
        sql = sql + " ,ST02_ST05_ID "
        sql = sql + " ,ST02_ST07_ID "
        sql = sql + " ,ST02_CD_LICENSE "
        sql = sql + " ,ST02_MN_DB "
        sql = sql + " ,ST02_NR_STREET "
        sql = sql + " ,ST02_MN_STREET "
        sql = sql + " ,ST02_MN_ADDRESS "
        sql = sql + " ,ST02_MN_ADDRESS2 "
        sql = sql + " ,ST02_CD_ZIP "
        sql = sql + " ,ST02_NR_SQUARE_FOOTAGE "
        sql = sql + " ,ST02_CD_LOGITUDE "
        sql = sql + " ,ST02_CD_LATITUDE "
        
        sql = sql + ") VALUES ( " 
        sql = sql + str(idEntity) + ","
        sql = sql + str(idCounty) + ","
        sql = sql + str(idType) + ","
        sql = sql + str(idEstType) + ","
        sql = sql + str(idCity) + ","
        sql = sql + row[1].strip().replace("'", "''") + ","
        sql = sql + "'" + row[5].strip().replace("'", "''") + "',"
        sql = sql + "'" + row[6].strip().replace("'", "''") + "',"
        sql = sql + "'" + row[7].strip().replace("'", "''") + "',"
        sql = sql + "'" + row[8].strip().replace("'", "''") + "',"
        sql = sql + "'" + row[9].strip().replace("'", "''") + "',"
        sql = sql + row[12].strip().replace("'", "''") + ","
        sql = sql + row[13].strip().replace("'", "''") + ","

        # {'longitude': '-74.078999', 'needs_recoding': False, 
        # 'human_address': '{"address":"19 FORT PL","city":"STATEN ISLAND","state":"NY","zip":"10301"}', 
        # 'latitude': '40.641971'}
        
        if row[14] != "":
            jsonValue = row[14].replace("'","\"").replace("False","\"False\"").replace("\"{","{").replace("}\"","}")
            jsn = JSON.loads(jsonValue)        
            
            if 'longitude' in jsn:
                sql = sql + "'" + jsn["longitude"] + "',"
            else:
                sql = sql + "'0.0',"
            
            if 'latitude' in jsn:
                sql = sql + "'" + jsn["latitude"] + "'"
            else:
                sql = sql + "'0.0'"
        else:
            sql = sql + "'0.0',"
            sql = sql + "'0.0'"
            
        sql = sql + ")"

        print(sql)
        self.cnn.insert(sql)

    def selectState(self, value):

        result = self.cnn.select("SELECT ST06_ID FROM [DB_STORES].[dbo].[ST06_STATE] WHERE ST06_SG_NAME = '"+value+"'")

        if result is not None:
            return result[0][0]
        else:
            return None
    
    def selectCity( self, idState, value):
        
        if idState is None: 
            return
        
        if value is None: 
            return

        result = self.cnn.select("SELECT ST07_ID FROM [DB_STORES].[dbo].[ST07_CITY] WHERE ST07_ST06_ID = "+str(idState)+" AND ST07_NM_NAME = '"+value+"'")
        if result is not None:
            return result[0][0]
        else:
            return None


    def selectEstablishimentType(self, value):
        if value is None: 
            return
        
        result = self.cnn.select("SELECT ST05_ID FROM [DB_STORES].[dbo].[ST05_ESTABLISHMENT_TYPE] WHERE ST05_NM_NAME = '"+value+"'")
        if result is not None:
            return result[0][0]
        else:
            return None

    def selectOperationType(self, value):
        if value is None: 
            return

        result = self.cnn.select("SELECT ST04_ID FROM [DB_STORES].[dbo].[ST04_OPERATION_TYPE] WHERE ST04_NM_NAME = '"+value+"'")
        if result is not None:
            return result[0][0]
        else:
            return None

    def selectCounty(self, value):
        if value is None: 
            return 

        result = self.cnn.select("SELECT ST03_ID FROM [DB_STORES].[dbo].[ST03_COUNTY] WHERE ST03_NM_NAME = '"+value+"'")
        if result is not None:
            return result[0][0]
        else:
            return None

    def selectEntity(self, value):
        if value is None: 
            return

        result = self.cnn.select("SELECT ST01_ID FROM [DB_STORES].[dbo].[ST01_ENTITY] WHERE ST01_NM_NAME = '"+value+"'")
        if result is not None:
            return result[0][0]
        else:
            None
